#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"

void member_area(user_t *u) {
	int choice;
	char name[50];
	do {
		printf("\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:
				printf("Enter book name");
				scanf("%s",name);
				book_find_by_name(name);
				break;
			case 2:
				edit_profile(u);
				break;
			case 3:
				change_password(u);
				break;
			case 4:
				bookcopy_checkavail();
				break;
		}
	}while (choice != 0);	
}

void bookcopy_checkavail(){
	 int book_id;
	 FILE *fp;
	 bookcopy_t bc;
	 int count = 0;
	 printf("enter book id :");
	 scanf("%d",&book_id);
	 fp = fopen(BOOKCOPY_DB,"rb");
	 if(fp == NULL){
		 perror("Cannot open bookcopies file.");
		 return ;
	 }
	 while(fread(&bc,sizeof(bookcopy_t),1,fp)>0){
		 if(bc.bookid == book_id && strcmp(bc.status,STATUS_AVAIL)==0){
			 count ++;
		 }
	 }

	 fclose(fp);
	printf("No of copies available : %d",count);
 }