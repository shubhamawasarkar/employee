#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"

void owner_area(user_t *u){
    int choice;
    do{
        printf("\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
        scanf("%d",&choice);
        switch(choice){
            case 1:
                appoint_librarian();
                break;
            case 2:
                edit_profile(u);
                break;
            case 3:
                change_password(u);
                break;
            case 4:
                break;
            case 5:
                book_availabilty();
                break;
            case 6:
                break;
            case 7:
                break;
        }
    }while(choice != 0);
}

void appoint_librarian(){
    user_t u;
    user_accept(&u);
    strcpy(u.role,ROLE_LIBRARIAN);
    user_add(&u);
}

void book_availabilty(){
    book_t t;
    FILE *f;
    f=fopen(BOOK_DB,"rb");
    if(f == NULL){
        perror("CANNOT OPEN FILE.");
        return;
    }
    while(fread(&t,sizeof(book_t),1,f)>0){
        int avail_count=0,issued_count=0,count=0;
        bookcopy_t b;
        FILE *fp;
        fp = fopen(BOOKCOPY_DB,"rb");
        if(fp == NULL){
            perror("CANNOT OPEN FILE.");
            return;
        }
        while(fread(&b,sizeof(bookcopy_t),1,fp)>0){
            if(t.id == b.bookid){
                ++count;
                if(strcmp(b.status,STATUS_AVAIL)==0){
                   ++avail_count;
                }
                else if(strcmp(b.status,STATUS_ISSUED)==0){
                    ++issued_count;
                }
            }
        }
         printf("\n \nId :%d, Name : %s, Available : %d, Issued :%d, Count : %d",t.id,t.name,avail_count,issued_count,count);
       fclose(fp);
    }
    fclose(f);
}
