#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"

void librarian_area(user_t *u){
    int choice,memberid;
	char name[50];
	do {
		printf("\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\n14. Display all members\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:
				add_member();
				break;
			case 2:
				edit_profile(u);
				break;
			case 3:
				change_password(u);
				break;
			case 4:
				add_book();
				break;
			case 5:
				printf("Enter book name :");
				scanf("%s",name);
				book_find_by_name(name);
				break;
			case 6:
				book_edit_by_id();
				break;
			case 7:
				bookcopy_checkavail_details();
				break;
			case 8:
				bookcopy_add();
				break;
			case 9:
				change_rack();
				break;
			case 10:
				bookcopy_issue();
				break;
			case 11:
				
				bookcopy_return();
				break;
			case 12:
				fees_payment_add();
				break;
			case 13:
				printf("Enter member id :");
				scanf("%d",&memberid);
				payment_history(memberid);
				break;
			case 14:
				display_members();
				break;
		}
	}while (choice != 0);		

}

void add_member(){
    user_t u;
    user_accept(&u);
    user_add(&u);
}

void add_book(){
	book_t b;
    book_accept(&b);
	b.id = get_next_book_id();
    FILE *fp;
	fp = fopen(BOOK_DB,"ab");
	if(fp == NULL){
		perror("Cannot open books file.");
		exit(1);
	}
	fwrite(&b,sizeof(book_t),1,fp);
	printf("Book Added in file.");
	fclose(fp);	
}

void book_edit_by_id(){
	int id, found = 0 ;
	FILE *fp;
	book_t b;
	printf("Enter book id :");
	scanf("%d",&id);
	fp = fopen(BOOK_DB,"rb+");
	if(fp == NULL){
		perror("Cannot open books file.\n");
		exit(1);
	}
	while(fread(&b,sizeof(book_t),1,fp)>0){
		if(id==b.id){
			found = 1;
			break;
		}
	}
	if(found){
		long size = sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.id = b.id;
		fseek(fp,-size,SEEK_CUR);
		fwrite(&nb,size,1,fp);
		printf("Book Updated.\n");
	}
	else
		printf("Book not found.\n");
	fclose(fp);
}

void bookcopy_add(){
	bookcopy_t b;
    bookcopy_accept(&b);
	b.id = get_next_bookcopy_id();
    FILE *fp;
	fp = fopen(BOOKCOPY_DB,"ab");
	if(fp == NULL){
		perror("Cannot open book copies file.");
		exit(1);
	}
	fwrite(&b,sizeof(bookcopy_t),1,fp);
	printf("Bookcopy  Added in file.");
	fclose(fp);	
}

 void bookcopy_checkavail_details(){
	 int book_id;
	 FILE *fp;
	 bookcopy_t bc;
	 int count = 0;
	 printf("enter book id :");
	 scanf("%d",&book_id);
	 fp = fopen(BOOKCOPY_DB,"rb");
	 if(fp == NULL){
		 perror("Cannot open bookcopies file.");
		 return ;
	 }
	 while(fread(&bc,sizeof(bookcopy_t),1,fp)>0){
		 if(bc.bookid == book_id && strcmp(bc.status,STATUS_AVAIL)==0){
			 bookcopy_display(&bc);
			 count ++;
		 }
	 }

	 fclose(fp);
	 if(count ==0)
		printf("no copies available;");
 }

 void bookcopy_issue(){
	 FILE *fp;
	issuerecord_t rec;
	issuerecord_accept(&rec);
	if(!is_paid_member(rec.memberid)){
		printf("Member is not paid\n");
		return;
	}
	rec.id = get_next_issuerecord_id();
	fp = fopen(ISSUERECORD_DB,"ab");
	if(fp == NULL){
		perror("CANNOT OPEN FILE .");
		exit(1);
	}
	fwrite(&rec,sizeof(issuerecord_t),1,fp);
	fclose(fp);

	bookcopy_changestatus(rec.copyid,STATUS_ISSUED);

 }
 
void bookcopy_changestatus(int bookcopy_id,char status[]){
	bookcopy_t bc;
	FILE *fp;
	long size = sizeof(bookcopy_t);
	fp = fopen(BOOKCOPY_DB,"rb+");
	if(fp == NULL){
		perror("CANNOT OPEN BOOKCOPIES FILE");
		return;
	}
	while(fread(&bc,sizeof(bookcopy_t),1,fp)>0){
		if(bookcopy_id == bc.id){
			strcpy(bc.status,status);
			fseek(fp,-size,SEEK_CUR);
			fwrite(&bc,sizeof(bookcopy_t),1,fp);
			break;
		}
	}
	fclose(fp);

}

void display_issued_bookcopies(int member_id){
	FILE *fp;
	issuerecord_t rec;
	fp = fopen(ISSUERECORD_DB,"rb");
	if(fp == NULL){
		perror("CANNOT OPEN FILE.");
		return;
	}
	while(fread(&rec,sizeof(issuerecord_t),1,fp) > 0){
		if(member_id == rec.memberid && rec.return_date.day == 0){
			issuerecord_display(&rec);
		}
	}
	fclose(fp);
}
void bookcopy_return(){
	FILE *fp;
	int member_id, record_id;
	issuerecord_t rec;
	int diff_days,found=0;
	long size = sizeof(issuerecord_t);
	printf("Enter member id :");
	scanf("%d",&member_id);
	display_issued_bookcopies(member_id);
	printf("Enter issue record id (to be returned) :");
	scanf("%d",&record_id);
	fp = fopen(ISSUERECORD_DB,"rb+");
	if(fp == NULL){
		perror("CANNOT OPEN FILE.");
		return;
	}
	while(fread(&rec,sizeof(issuerecord_t),1,fp)>0){
		if(record_id == rec.id){
			rec.return_date = date_current();
			found = 1;
			diff_days = date_cmp(rec.return_date,rec.return_duedate);
			if(diff_days > 0){
				rec.fine_amount = diff_days * FINE_PER_DAY;
				fine_payment_add(rec.memberid,rec.fine_amount);
				printf("Fine amount Rs..2lf is applied",rec.fine_amount);
			}
			break;
		}
	}
	if(found){
		fseek(fp,-size,SEEK_CUR);
		fwrite(&rec,sizeof(issuerecord_t),1,fp);
		printf("Issuerecord updated after returning book\n");
		issuerecord_display(&rec);
		bookcopy_changestatus(rec.copyid,STATUS_AVAIL);
	}
	fclose(fp);

}

//display all members
void display_members(){
	FILE *fp;
	user_t u;

	fp = fopen(USER_DB,"rb");
	if(fp == NULL){
		perror("CANNOT OPEN FILE");
		return;
	}
	while(fread(&u,sizeof(user_t),1,fp)>0){
		user_display(&u);
	}
	fclose(fp);
}


//change rack
void change_rack(){
	FILE *fp;
	int cid,rack;
	bookcopy_t b;
	long size = sizeof(bookcopy_t);
	printf("Enter bookcopy ID :");
	scanf("%d",&cid);
	fp= fopen(BOOKCOPY_DB,"rb+");
	if(fp == NULL){
		perror("CANNOT OPEN FILE.");
		return;
	}
	while(fread(&b,sizeof(bookcopy_t),1,fp)>0){
		if(cid == b.id){
			printf("Enter new rack id :");
			scanf("%d",&rack);
			b.rack = rack ;
			fseek(fp,-size,SEEK_CUR);
			fwrite(&b,sizeof(bookcopy_t),1,fp);
			break;
		}
	}
	fclose(fp);

}

void fees_payment_add(){
	FILE *fp;
	payment_t pay;
	pay.id = get_next_payment_id();
	payment_accept(&pay);
	fp = fopen(PAYMENT_DB,"ab");
	if(fp == NULL){
		perror("CANNOT OPEN PAYMENT FILE.");
		return;
	}
	fwrite(&pay,sizeof(payment_t),1,fp);
	fclose(fp);
}

void payment_history(int memberid){
	FILE *fp;
	payment_t pay;
	fp = fopen(PAYMENT_DB,"rb");
	if(fp == NULL){
		perror("CANNOT OPEN PAyMEnt filE");
		return;
	}

	while(fread(&pay,sizeof(payment_t),1,fp)>0){
		if(pay.memberid == memberid)
			payment_display(&pay);
	}
	fclose(fp);
}

int is_paid_member(int memberid){
	date_t now = date_current();
	int paid = 0;
	FILE *fp;
	payment_t pay;
	fp = fopen(PAYMENT_DB,"rb");
	if(fp == NULL){
		perror("CANNOT OPEN PAyMEnt filE");
		return 0;
	}

	while(fread(&pay,sizeof(payment_t),1,fp)>0){
		if(pay.memberid == memberid && pay.next_pay_duedate.day != 0 && date_cmp(now,pay.next_pay_duedate) < 0){
			paid =1;
			break;
		}
	}
	fclose(fp);
	return paid;
}

void fine_payment_add(int memberid,double fine_amount){
	FILE *fp;
	payment_t pay;
	pay.id = get_next_payment_id();
	pay.memberid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.type,PAY_TYPE_FINE);
	pay.tx_time = date_current();
	memset(&pay.next_pay_duedate,0,sizeof(date_t));
	fp = fopen(PAYMENT_DB,"ab");
	if(fp == NULL){
		perror("CANNOT OPEN PAYMENT FILE.");
		return;
	}
	fwrite(&pay,sizeof(payment_t),1,fp);
	fclose(fp);
}