#include<stdio.h>
#include<string.h>

#include "library.h"

//user functions
void user_accept(user_t *u)
{
    //printf("ID :");
    //scanf("%d",&u->id);
    u->id = get_next_user_id();
    printf(" Name :");
    scanf("%s",u->name);
    printf(" E-mail :");
    scanf("%s",u->email);
    printf(" Phone :");
    scanf("%s",u->phone);
    printf(" password :");
    scanf("%s",u->password);
    strcpy(u->role,ROLE_MEMBER);
}
void user_display(user_t  *u)
{
    printf("\n\n ID : %d",u->id);
    printf("\n Name : %s",u->name);
    printf("\n Mail : %s",u->email);
    printf("\n Phone : %s",u->phone);
    printf("\n Role : %s",u->role);
}

//edit profilr
void edit_profile(user_t *u){
    long size = sizeof(user_t);
    FILE *fp;
    user_t t;
    fp = fopen(USER_DB,"rb+");
	if(fp == NULL){
		perror("Cannot open books file.\n");
		return;
	}
	while(fread(&t,sizeof(user_t),1,fp)>0){
		if(u->id == t.id){
            if(strcmp(u->role,ROLE_OWNER) != 0){
			    printf("New email :");
                scanf("%s",&t.email);
            }
            printf("New phone :");
            scanf("%s",&t.phone);
            fseek(fp,-size,SEEK_CUR);
            fwrite(&t,size,1,fp);
	        printf("Profile updated successfully.\n");
			break;
		}
	}
fclose(fp);
}

//change password
void change_password(user_t *u){
     long size = sizeof(user_t);
    FILE *fp;
    user_t t;
    fp = fopen(USER_DB,"rb+");
	if(fp == NULL){
		perror("Cannot open books file.\n");
		return;
	}
	while(fread(&t,sizeof(user_t),1,fp)>0){
		if(u->id == t.id){
			printf("New Password :");
            scanf("%s",&t.password);
            fseek(fp,-size,SEEK_CUR);
            fwrite(&t,size,1,fp);
	        printf("Password updated successfully.\n");
			break;
		}
	}
fclose(fp);
}

//book functioons
void book_accept(book_t *b)
{
    //printf("\n ID :");
    //scanf("%d",&b->id);

    printf(" Name :");
    scanf("%s",b->name);
    printf(" Author :");
    scanf("%s",b->author);
    printf(" Price :");
    scanf("%lf",&b->price);
    printf(" Subject :");
    scanf("%s",b->subject);
    printf(" ISBN :");
    scanf("%s",b->isbn);
}

void book_display(book_t *b){
    printf("\n ID : %d",b->id);
    printf("\n Name : %s",b->name);
    printf("\n Author : %s",b->author);
    printf("\n Price : %.2lf",b->price);
    printf("\n Subject : %s",b->subject);
    printf("\n isbn : %s",b->isbn);
}

//bookcopy functions
void bookcopy_accept(bookcopy_t *c){
    //printf("\nID :");
    //scanf("%d",&c->id);
    printf("\nBook ID :");
    scanf("%d",&c->bookid);
    printf("\nRack :");
    scanf("%d",&c->rack);
    strcpy(c->status,STATUS_AVAIL);

}

void bookcopy_display(bookcopy_t *c){
    printf("\nId : %d",c->id);
    printf("\nBook Id: %d",c->bookid);
    printf("\nRack : %d",c->rack);
    printf("\nStatus : %s",c->status);
}

//issuerecord functions
void issuerecord_accept(issuerecord_t *r){
    //printf("\nID :");
    //scanf("%d",&r->id);
    printf("\nCopy ID :");
    scanf("%d",&r->copyid);
    printf("\nMember Id :");
    scanf("%d",&r->memberid);
    printf("issue");
    date_accept(&r -> issue_date);
    //r->issue_date = date_current(); To automate issue date
    r->return_duedate = date_add(r->issue_date,BOOK_RETURN_DAYS);
    memset(&r->return_date,0,sizeof(date_t));
    r->fine_amount = 0.0;

}

void issuerecord_display(issuerecord_t *r){
    printf("\nIssue Id :%d",r->id);
    printf("\nCopy Id:%d",r->copyid);
    printf("\nMember Id %d:",r->memberid);
    printf("\nFine amount :%.2lf\n",r->fine_amount);
    printf("issue");
    date_print(&r->issue_date);
    printf("return due");
    date_print(&r->return_duedate);
    printf("retun");
    date_print(&r->return_date);
}

//payment functions
void payment_accept(payment_t *p){
    //printf("\nID :");
    //scanf("%d",&p->id);

    printf("\nMember ID :");
    scanf("%d",&p->memberid);
 //   printf("\ntype(fine/fees) :");
  //  scanf("%s",p->type);
    strcpy(p->type,PAY_TYPE_FEES);
    printf("Amount :");
    scanf("%lf",&p->amount);
    p->tx_time = date_current();
  //  if(strcmp(p->type,PAY_TYPE_FEES)==0)
    p->next_pay_duedate = date_add(p->tx_time,MEMBERSHIP_MONTH_DAYS);
 //   else
  //      memset(&p->next_pay_duedate,0,sizeof(date_t));
    
}

void payment_display(payment_t *p){
    printf("payment: %d, member: %d, %s, amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
    printf("payment");
    date_print(&p->tx_time);
    printf("payment_due");
    date_print(&p->next_pay_duedate);
}

void user_add(user_t *u){
    FILE *fp;
    fp = fopen(USER_DB,"ab");
    if(fp == NULL){
        perror("Failed to open user file");
        return;
    }
    fwrite(u,sizeof(user_t),1,fp);
    printf("User Added");
    fclose(fp);
     
}

void book_find_by_name(char name[]){
    FILE *fp;
    int found = 0 ;
    fp = fopen(BOOK_DB,"rb");
    book_t b;
    if(fp == NULL){
        perror("Failed to open books file");
        return;
    }
    while(fread(&b,sizeof(book_t),1,fp)>0){

        if(strstr(b.name,name) != NULL){
            found=1;
            book_display(&b);
      }
    }
    fclose(fp);
    if(!found)
        printf("No such book in library");
}

int user_find_by_email(user_t *u,char email[]){
    FILE *fp;
    int found = 0 ;
    fp = fopen(USER_DB,"rb");
    if(fp == NULL){
        perror("Failed to open user file");
        return found;;
    }
    while(fread(u,sizeof(user_t),1,fp)>0){
        if(strcmp(u->email,email)==0){
            found=1;
            break;
      }
    }
    fclose(fp);
    return found;
}

int get_next_user_id(){
    FILE *fp;
    int max=0;
    int size = sizeof(user_t);
    user_t u;
    fp = fopen(USER_DB, "rb");
    if(fp==NULL)
        return max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&u,size,1,fp)>0)
        max = u.id;
    fclose(fp);
    return max+1;
}

int get_next_book_id(){
    FILE *fp;
    int max=0;
    int size = sizeof(book_t);
    book_t u;
    fp = fopen(BOOK_DB, "rb");
    if(fp==NULL)
        return max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&u,size,1,fp)>0)
        max = u.id;
    fclose(fp);
    return max+1;
}

int get_next_bookcopy_id(){
    FILE *fp;
    int max=0;
    int size = sizeof(bookcopy_t);
    bookcopy_t u;
    fp = fopen(BOOKCOPY_DB, "rb");
    if(fp==NULL)
        return max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&u,size,1,fp)>0)
        max = u.id;
    fclose(fp);
    return max+1;
}

int get_next_issuerecord_id(){
    FILE *fp;
    int max=0;
    int size = sizeof(issuerecord_t);
    issuerecord_t u;
    fp = fopen(ISSUERECORD_DB, "rb");
    if(fp==NULL)
        return max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&u,size,1,fp)>0)
        max = u.id;
    fclose(fp);
    return max+1;
}


int get_next_payment_id(){
    FILE *fp;
    int max=0;
    int size = sizeof(payment_t);
    payment_t u;
    fp = fopen(PAYMENT_DB, "rb");
    if(fp==NULL)
        return max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&u,size,1,fp)>0)
        max = u.id;
    fclose(fp);
    return max+1;
}